from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

import random
import time
from datetime import datetime
#for ping command
import os

# Global constant
REGISTER_PAGE = "https://mcdwifi.y5zone.net/live/Portal/en"
WELCOME_PAGE = "https://mcdwifi.y5zone.net/live/Portal/en/Welcome"

class MC_wifi_bot:

    def __init__(self, login_page, welcome_page, timeout=10, email_sp='gmail.com'):
        self.email_sp = email_sp
        self.reg_page = login_page
        self.wel_page = welcome_page
        self.timeout = timeout
        #future work: implementing other webdrivers
        self.driver = None
    
    def spawn(self, flavour=0):
        if flavour == 0:
            self.driver = webdriver.Firefox()
        else:
            raise Exception("flavour not found!")

    def destory(self):
        if self.driver:
            self.driver.quit()

    def generate_email(self, length, inc_num=True):
        table = "abcdefghijklmnopqrstuvwxyz"
        table += table.upper()
        if inc_num == True:
            table += '0123456789'
        char_arr = []
        for c in table:
            char_arr.append(c)
        del table
        username = ''.join(random.sample(char_arr, length))
        return username + '@' + self.email_sp

    def activate_wifi(self, length, inc_num=True):
        #destory existed driver
        self.destory()
        #spawn a new driver
        self.spawn()

        ##here's the real deal
        #go to mcdonalds wifi page
        self.driver.get(self.reg_page)

        #wait for element to be loaded
        email_field = None
        try:
            email_field = WebDriverWait(self.driver, self.timeout)\
            .until(ec.presence_of_element_located((By.ID, "textbox-email")))
        except TimeoutException:
            print("[{}] Cannot locate element 'textbox-email'".format(str(datetime.now())))
            self.destory()
            return False
        #check email_field
        if not email_field:
            return False
        
        #generate email
        email = self.generate_email(length, inc_num)
        #type in our random generated email
        email_field.send_keys(email)
        #perform the magic
        email_field.submit()

        ## So if everything is okay, we should be redirecting to welcome page
        # METHOD 1
        try:
            WebDriverWait(self.driver, self.timeout)\
            .until(ec.url_changes(self.wel_page))
        except TimeoutException:
            print("[{}] Timeout, dunno where I am :(".format(str(datetime.now())))
            self.destory()
            return False
        #finally we kill the broswer
        self.destory()
        return True

def ping_hostname(hostname, times=1, timeout=2):
    cmd = "ping -q -c {0} -W {1} {2}".format(times, timeout, hostname)
    return True if os.system(cmd) == 0 else False

if __name__ == '__main__':
    #create our bot
    bot = MC_wifi_bot(login_page=REGISTER_PAGE, welcome_page=WELCOME_PAGE, timeout=10, email_sp='gmail.com')
    print("### McDonald's free wifi automation ###")
    while True:
        print('[{0}] Alive, working...'.format(str(datetime.now())))
        if not ping_hostname('google.com'):
            print('[{0}] Cant reach to Google.'.format(str(datetime.now())))
            #try activate wifi service
            if bot.activate_wifi(16, True):
                print('[{}] Activation success!'.format(str(datetime.now())))
            else:
                print('[{}] Activation fail!'.format(str(datetime.now())))
        else:
            print('[{}] Connected to Internet'.format(str(datetime.now())))
        
        #put this thread to sleep
        print('[{}] Halt.'.format(str(datetime.now())))
        time.sleep(10 * 60)
        
